Ansible role: docker
=========

![pipeline](https://gitlab.com/tcblome/ansible-role-docker/badges/master/build.svg)

This role installs [Community Edition of Docker](https://www.docker.com/community-edition).


Requirements
------------
none

Installation
------------

Using `ansible-galaxy`:

```shell
$ ansible-galaxy install https://gitlab.com/tcblome/ansible-role-docker.git
```

Using `requirements.yml`:
```yaml
 src: git+https://gitlab.com/tcblome/ansible-role-docker.git
 name: tcblome.docker
```
Using `git`:
```shell
$ git clone https://gitlab.com/tcblome/ansible-role-docker.git tcblome.docker
```

Role Variables
--------------
```yaml

# defaults file for ansible-role-docker
#
# docker_version	*	# version of docker to install

```

Dependencies
------------

This role can be used independently.

Example Playbook
----------------

Sample :
```
    - hosts: servers
      roles:
        - { role: tcblome.docker,
	  docker_version: "18.03.0~ce-0~ubuntu"
	 }
```

License
------------------
[LICENSE](LICENSE)

Author Information
------------------

This role is published for private use by @tcblome
